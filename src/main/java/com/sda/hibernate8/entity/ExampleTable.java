package com.sda.hibernate8.entity;

import javax.persistence.*;

@Entity
@Table(name = "EXAMPLE_TABLE")
public class ExampleTable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column(name="some_column")
    private String someColumn;

    public ExampleTable(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSomeColumn() {
        return someColumn;
    }

    public void setSomeColumn(String someColumn) {
        this.someColumn = someColumn;
    }
}
